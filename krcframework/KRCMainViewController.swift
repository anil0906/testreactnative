//
//  KRCMainViewController.swift
//  KRCFramework
//
//  Created by Anil Sharma on 3/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//
//
import UIKit
import WebKit

public class KRCMainViewController: UIViewController {


   var webView: WKWebView!


    override public func loadView() {
        let contentController = WKUserContentController()
        // Can be used to pass initial info like rc config or deep linking url
        let scriptSource = "window.urlToBeLoaded = `/#/eventPage`"
        let script = WKUserScript(source: scriptSource, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        contentController.addUserScript(script)
//
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
//
        webView = WKWebView(frame: .zero, configuration: config)
        webView = WKWebView()
        view = webView
    }

    private func loadStaticPage() {
        if let htmlPath = Bundle(for: KRCMainViewController.self).path(forResource: "web/index", ofType: "html") {
                let url = URL(fileURLWithPath: htmlPath)
                let request = URLRequest(url: url)
                webView.load(request)
        } else {
            // show error page
        }
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        loadStaticPage()
    }


}


//
//  KrcRNViewController.swift
//  KRCFramework
//
//  Created by Anil Sharma on 5/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

//
//  ViewController.swift
//  TestReactIos
//
//  Created by Anil Sharma on 5/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

//import UIKit
//import React
//
//class KRCMainViewController: UIViewController {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view.
//
////    let jsCodeLocation = URL(string: "http://localhost:8081/index.bundle?platform=ios")!
//        let jsCodeLocation = Bundle(for: KRCMainViewController.self).url(forResource: "KindredRacing", withExtension: "bundle")
//        print(jsCodeLocation)
//    let mockData:NSDictionary = ["scores":
//        [
//            ["name":"Golu", "value":"42"],
//            ["name":"Molu", "value":"10"],
//            ["name":"Dolu", "value":"15"]
//        ]
//    ]
//
//    let rootView = RCTRootView(
//        bundleURL: jsCodeLocation as! URL,
//        moduleName: "RNHighScores",
//        initialProperties: mockData as [NSObject : AnyObject],
//        launchOptions: nil
//    )
//        self.view = rootView
//    }
//}


