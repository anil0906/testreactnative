//
//  KrcRNViewController.swift
//  KRCFramework
//
//  Created by Anil Sharma on 5/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

//
//  ViewController.swift
//  TestReactIos
//
//  Created by Anil Sharma on 5/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

import UIKit
import React
public class KRCRNViewController: UIViewController {

    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
//    let jsCodeLocation = URL(string: "http://localhost:8081/index.bundle?platform=ios")!
        let jsCodeLocation = Bundle(for: KRCRNViewController.self).url(forResource: "KindredRacing", withExtension: "bundle")
        print(jsCodeLocation)
    let mockData:NSDictionary = ["scores":
        [
            ["name":"Golu", "value":"42"],
            ["name":"Molu", "value":"10"],
            ["name":"Dolu", "value":"15"]
        ]
    ]

    let rootView = RCTRootView(
        bundleURL: jsCodeLocation as! URL,
        moduleName: "RNHighScores",
        initialProperties: mockData as [NSObject : AnyObject],
        launchOptions: nil
    )
        self.view = rootView
    }
}


